sudo apt update -y

sudo apt install python3-pip -y
python3 -m pip install --user pipx
export PATH=$PATH:/home/ubuntu/.local/bin

sudo apt install python3.10-venv -y
pipx install --include-deps ansible
ansible --version
