## Ansible

### Comandos
- Executar 
  - `ansible all --key-file ~/.ssh/ansible.pem -i hosts`
  - `ansible-playbook main.yaml -i hosts.ini --check --key-file=~/.ssh/iac-2024-azure.pem -vv`
- fazer um ping
  - `ansible all --key-file ~/.ssh/ansible.pem -i hosts -m ping` 
- gerar uma nova role
  - ansible-galaxy init nome_da_role