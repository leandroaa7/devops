resource "azurerm_linux_virtual_machine" "vm" {
  name                  = "${var.prefix}-vm"
  location              = var.location
  resource_group_name   = azurerm_resource_group.resource_group.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  size                  = "Standard_DS1_v2"
  computer_name         = "hostname"
  admin_username        = var.vm_user
  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }
  /*   os_profile {
    computer_name  = "hostname"
    admin_username = var.vm_user
    admin_password = var.vm_password
  } */
  os_disk {
    name              = "${var.prefix}-osdisk1"
    caching           = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
/*   os_profile_linux_config {
    disable_password_authentication = false
  } */
  tags = {
    environment = "Production"
  }

  admin_ssh_key {
    username   = var.vm_user
    public_key = "${file(var.azure_access_key)}"
  }

}
