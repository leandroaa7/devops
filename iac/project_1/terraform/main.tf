# main
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.0.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
  tenant_id       = var.tenant_id
}

resource "azurerm_resource_group" "resource_group" {
  name     = "${var.prefix}-rg-vms"
  location = var.location
}

# Virtual Network
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.prefix}-vnet"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  address_space       = ["10.0.0.0/16"]
}

# Subnet
resource "azurerm_subnet" "subnet" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.resource_group.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

# Public Ip

resource "azurerm_public_ip" "publicip" {
  name                = "pip1"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  allocation_method   = "Dynamic"
  sku                 = "Basic"
}

# Network interface
resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip.id
  }
}

# Security Group
resource "azurerm_network_security_group" "frontendnsg" {
  name                = "${var.prefix}-security-group"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  /*   tags = {
    environment = "Production"
  } */
}

# Security rule HTTP
resource "azurerm_network_security_rule" "httprule" {
  name                        = "${var.prefix}-http"
  resource_group_name         = azurerm_resource_group.resource_group.name
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "8080"
  source_address_prefix       = "*"
  destination_address_prefix  = "10.0.1.0/24"
  network_security_group_name = azurerm_network_security_group.frontendnsg.name
}

# Security rule SSH
resource "azurerm_network_security_rule" "sshrule" {
  name                        = "${var.prefix}-ssh"
  resource_group_name         = azurerm_resource_group.resource_group.name
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  destination_address_prefix  = "10.0.1.0/24"
  network_security_group_name = azurerm_network_security_group.frontendnsg.name
  source_address_prefix       = "*"
}

# Subnet - Security group association
resource "azurerm_subnet_network_security_group_association" "subnetsecuritygroup" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.frontendnsg.id
}
