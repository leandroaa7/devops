/* variable "client_secret" {
  type        = string
  description = "Enter your client secret"
}

variable "client_id" {
  type        = string
  description = "Your client id"
} */

variable "subscription_id" {
  type        = string
  default     = ""
  description = "Your subscription id"
}

variable "tenant_id" {
  type        = string
  default     = ""
  description = "Your tenant id"
}

variable "location" {
  type        = string
  default     = "East US"
  description = "The Azure region in which all resources"
}

variable "prefix" {
  type        = string
  default     = "dev"
  description = "The prefix which should be used for all resources"
}

variable "vm_user" {
  default = "ubuntu"
  type = string
  description = "The username of VM"
}

variable "vm_password" {
  default = "ubuntu"
  type = string
  description = "The password of VM"
}

variable "vm_ssh_file_name" {
  type = string
  description = "The name of public ssh file"
  default = "id_rsa.pub"
}

variable "vm_ssh_path" {
  type = string
  description = "The path of ssh file without last /, example ./env"
  default = "./env"
}

variable "azure_access_key" {
  type        = string
  description = "public ssh key"
}
